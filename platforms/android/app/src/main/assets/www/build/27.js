webpackJsonp([27],{

/***/ 668:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TollPageModule", function() { return TollPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toll__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TollPageModule = /** @class */ (function () {
    function TollPageModule() {
    }
    TollPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__toll__["a" /* TollPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__toll__["a" /* TollPage */]),
            ],
        })
    ], TollPageModule);
    return TollPageModule;
}());

//# sourceMappingURL=toll.module.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TollPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TollPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TollPage = /** @class */ (function () {
    function TollPage(navCtrl, navParams, apicalldaywise) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaywise = apicalldaywise;
        this.toggled = false;
        this.toggled = false;
        this.tollList();
    }
    TollPage.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    TollPage.prototype.cancelSearch = function () {
        // this.toggle();
        this.toggled = false;
    };
    TollPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        //_baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        _baseURL = "https://www.oneqlik.in/toll/get?search=" + searchKey;
        this.apicalldaywise.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.list = data;
            //this.allDevices = data.devices;
            // console.log("fuel percentage: " + data.devices[0].fuel_percent)
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    TollPage.prototype.onClear = function (ev) {
        this.tollList();
        // this.getdevicesTemp();
        ev.target.value = '';
        // this.toggled = false;
    };
    TollPage.prototype.live = function () {
        this.navCtrl.push("TollMapPage");
    };
    TollPage.prototype.tollList = function () {
        var _this = this;
        console.log("inside", 123);
        var baseURLp = "https://www.oneqlik.in/toll/get";
        this.apicalldaywise.startLoading().present();
        this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalldaywise.stopLoading();
            console.log("toll list", data);
            _this.list = data;
        }, function (err) {
            _this.apicalldaywise.stopLoading();
            console.log(err);
        });
    };
    TollPage.prototype.addTolls = function () {
        this.navCtrl.push('TollAddTollPage');
    };
    TollPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TollPage');
    };
    TollPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-toll',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/toll/toll.html"*/'<!--\n\n  Generated template for the TollPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Toll List</ion-title>\n\n    <!-- <ion-buttons end>\n\n      <button  ion-button icon-only>\n\n\n\n        <img src="assets/icon/stack-of-square-papers-svg.png">\n\n      </button>\n\n    </ion-buttons> -->\n\n\n\n    <ion-buttons end>\n\n      <button (click) = "live()" end>  Map View </button>\n\n      </ion-buttons>\n\n\n\n    <!-- <ion-buttons end>\n\n      <div>\n\n        <ion-icon name="radio-button-on"></ion-icon>\n\n        <ion-icon style="font-size: 2.2em;" color="light" *ngIf="!toggled" (click)="toggle()" name="search"></ion-icon>\n\n        <ion-searchbar *ngIf="toggled" (ionInput)="callSearch($event)" (ionClear)="onClear($event)"\n\n          (ionCancel)="cancelSearch($event)"></ion-searchbar>\n\n      </div>\n\n    </ion-buttons> -->\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content no-padding>\n\n<!-- <ion-buttons>\n\n<button (click) = "live()" end>  Map View </button>\n\n</ion-buttons> -->\n\n    <ion-card *ngFor="let item of list; let i = index">\n\n  <ion-item style="border-bottom: 2px solid #dedede;">\n\n\n\n    <ion-row>\n\n      <ion-col col-3>\n\n        <img src="assets/imgs/toll.png" style="\n\n          margin-top: -10px;\n\n          width: 40px;\n\n          height: 40px;" />\n\n      </ion-col>\n\n      <ion-col col-3>\n\n        <p style="\n\n          color:black;\n\n          font-size:16px;\n\n          margin: 0px;">\n\n          {{ item.tollName }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: end;">\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n         ">\n\n          {{item.tollTax}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n\n\n        ">\n\n          {{\'Toll Tax\'}}\n\n        </p>\n\n      </ion-col>\n\n      <!-- <ion-col col-4>\n\n        <p style="\n\n          text-align: center;\n\n          font-size: 11px;\n\n          color:#11c1f3;\n\n          font-weight:bold;">\n\n          {{item.Date}}\n\n        </p>\n\n      </ion-col> -->\n\n\n\n    </ion-row>\n\n\n\n    <!-- <ion-row style="margin-top:2%;">\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n         ">\n\n          {{item.tollTax}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        ">\n\n          {{\'Toll Tax\'}}\n\n        </p>\n\n      </ion-col>\n\n\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;">\n\n          {{item.location.coordinates[\'0\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:#53ab53;\n\n        font-weight:bold;">\n\n          {{\'Latitude\' }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n          margin-left: 8px;">\n\n          {{item.location.coordinates[\'1\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        margin-left: 8px;">\n\n          {{\'Longitude\'}}\n\n        </p>\n\n      </ion-col>\n\n    </ion-row> -->\n\n\n\n    <ion-row style="margin-top:2%;">\n\n\n\n      <ion-col col-4>\n\n        <p style="\n\n        color:gray;\n\n        font-size:11px;\n\n        font-weight:400;\n\n       ">\n\n          {{item.address}}\n\n        </p>\n\n\n\n        <p style="\n\n      color:#53ab53;\n\n      font-size: 11px;\n\n      font-weight: bold;\n\n      ">\n\n          {{\'Address\' }}\n\n        </p>\n\n      </ion-col>\n\n\n\n      <!-- <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;">\n\n          {{item.location.coordinates[\'0\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:#53ab53;\n\n        font-weight:bold;">\n\n          {{\'Latitude\' }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n          margin-left: 8px;">\n\n          {{item.location.coordinates[\'1\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        margin-left: 8px;">\n\n          {{\'Longitude\'}}\n\n        </p>\n\n      </ion-col> -->\n\n\n\n    </ion-row>\n\n\n\n\n\n    <!--\n\n    -->\n\n\n\n    <!-- </ion-scroll> -->\n\n\n\n\n\n  </ion-item>\n\n </ion-card>\n\n  <!-- <ion-fab right bottom>\n\n    <button ion-fab color="gpsc" (click)="addTolls()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab> -->\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/toll/toll.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], TollPage);
    return TollPage;
}());

//# sourceMappingURL=toll.js.map

/***/ })

});
//# sourceMappingURL=27.js.map