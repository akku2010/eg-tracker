webpackJsonp([28],{

/***/ 665:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportedDevicesPageModule", function() { return SupportedDevicesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__supported_devices__ = __webpack_require__(785);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SupportedDevicesPageModule = /** @class */ (function () {
    function SupportedDevicesPageModule() {
    }
    SupportedDevicesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__supported_devices__["a" /* SupportedDevicesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__supported_devices__["a" /* SupportedDevicesPage */]),
            ],
        })
    ], SupportedDevicesPageModule);
    return SupportedDevicesPageModule;
}());

//# sourceMappingURL=supported-devices.module.js.map

/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportedDevicesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detailed_detailed__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SupportedDevicesPage = /** @class */ (function () {
    function SupportedDevicesPage(navCtrl, navParams, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.devices = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    SupportedDevicesPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter SupportedDevicesPage');
        this.getSupportedDevices();
    };
    SupportedDevicesPage.prototype.showDetail = function (d) {
        console.log('check d here: ', d);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__detailed_detailed__["a" /* DetailedPage */], {
            param: d
        });
    };
    SupportedDevicesPage.prototype.getSupportedDevices = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "product/getProduct?postedBy=59cbbdbe508f164aa2fef3d8";
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response data: ", data);
            _this.devices = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("got error: ", err);
        });
    };
    SupportedDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-supported-devices',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/supported-devices/supported-devices.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Supported Devices</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <!-- <ion-list>\n\n    <ion-item (click)="showDetail()">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/tractor_blue.png" />\n\n      </ion-avatar>\n\n      <h2>Finn</h2>\n\n      <h3>Don\'t Know What To Do!</h3>\n\n      <p>I\'ve had a pretty messed up day. If we just...</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/tractor_blue.png" />\n\n      </ion-avatar>\n\n      <h2>Cher</h2>\n\n      <p>Ugh. As if. mnsd zknk ncksnc sdncklsmcl snksndksndlksa asndlaskndla</p>\n\n    </ion-item>\n\n  </ion-list> -->\n\n  <ion-grid *ngIf="devices.length > 0">\n\n  <ion-row>\n\n    <ion-col col-6 *ngFor="let d of devices"  (click)="showDetail(d)">\n\n      <div class="ionCol">\n\n          <div class=\'box\'>\n\n              <img src="assets/slides/img1.jpg" />\n\n              <div id=\'noti-count\'>\n\n                <div>{{d.discount_price}}% off</div>\n\n              </div>\n\n            </div>\n\n            <h5>{{d.Product_name}}</h5>\n\n            <p>\n\n              <span>\n\n                <ion-icon name="custom-rupee" style="font-size: 0.9em;"></ion-icon> \n\n                <span style="font-size: 1.1em;">{{d.discount_price}}</span>\n\n              </span>\n\n              <span style="text-decoration: line-through;">\n\n                  <ion-icon name="custom-rupee" style="font-size: 0.9em;"></ion-icon> \n\n                  <span style="font-size: 1.1em;">{{d.price}}</span>\n\n                </span>\n\n            </p>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/supported-devices/supported-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], SupportedDevicesPage);
    return SupportedDevicesPage;
}());

//# sourceMappingURL=supported-devices.js.map

/***/ })

});
//# sourceMappingURL=28.js.map