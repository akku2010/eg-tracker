webpackJsonp([33],{

/***/ 656:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteVoilationsPageModule", function() { return RouteVoilationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__route_voilations__ = __webpack_require__(775);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var RouteVoilationsPageModule = /** @class */ (function () {
    function RouteVoilationsPageModule() {
    }
    RouteVoilationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__route_voilations__["a" /* RouteVoilationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__route_voilations__["a" /* RouteVoilationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], RouteVoilationsPageModule);
    return RouteVoilationsPageModule;
}());

//# sourceMappingURL=route-voilations.module.js.map

/***/ }),

/***/ 775:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouteVoilationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RouteVoilationsPage = /** @class */ (function () {
    function RouteVoilationsPage(navCtrl, navParams, apicallroute, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallroute = apicallroute;
        this.alertCtrl = alertCtrl;
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    RouteVoilationsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter RouteVoilationsPage');
    };
    RouteVoilationsPage.prototype.ngOnInit = function () {
        this.getRoute();
    };
    RouteVoilationsPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    RouteVoilationsPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    RouteVoilationsPage.prototype.getRoute = function () {
        var _this = this;
        var baseURLp = this.apicallroute.mainUrl + 'trackRoute/user/' + this.islogin._id;
        this.apicallroute.startLoading().present();
        this.apicallroute.getallrouteCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallroute.stopLoading();
            _this.devices1243 = [];
            _this.routelist = data;
            console.log("Routelist=> ", _this.routelist);
        }, function (err) {
            _this.apicallroute.stopLoading();
            console.log(err);
        });
    };
    RouteVoilationsPage.prototype.getRouteName = function (from, to, selectedroute) {
        console.log("selectedVehicle=> ", selectedroute);
        this.routename_id = selectedroute.Device_Name;
    };
    RouteVoilationsPage.prototype.getroutevoilation = function (starttime, endtime) {
        var _this = this;
        var baseURLp = this.apicallroute.mainUrl + 'notifs/RouteVoilationReprot?from_date=' + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id;
        this.apicallroute.startLoading().present();
        this.apicallroute.getallrouteCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallroute.stopLoading();
            _this.routevolitionReport = data;
            console.log(_this.routevolitionReport);
            if (_this.routevolitionReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallroute.stopLoading();
            console.log(error);
        });
    };
    RouteVoilationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-route-voilations',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/route-voilations/route-voilations.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{\'Route Violation Report\' | translate}}</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">{{\'Select Route\' | translate}}</ion-label>\n\n    <select-searchable\n\n      item-content\n\n      [(ngModel)]="selectedRoute"\n\n      [items]="routelist"\n\n      itemValueField="name"\n\n      itemTextField="name"\n\n      [canSearch]="true"\n\n      (onChange)="getRouteName(datetimeStart, datetimeEnd, selectedRoute)"\n\n    >\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [min]="twoMonthsLater" [max]="today"\n\n          [(ngModel)]="datetimeStart"\n\n          (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n\n        ></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [(ngModel)]="datetimeEnd"\n\n          (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n\n        ></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon\n\n          ios="ios-search"\n\n          md="md-search"\n\n          style="font-size:2.3em;"\n\n          (click)="getroutevoilation(datetimeStart, datetimeEnd)"\n\n        ></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-card *ngFor="let routedata of routevolitionReport">\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png" />\n\n        </ion-avatar>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="margin-left:3px;color:black;font-size:16px;">\n\n              {{ routedata.vehicleName }}\n\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <p style="margin-top:0%;">\n\n            <span>\n\n              <ion-icon\n\n                ios="ios-time"\n\n                md="md-time"\n\n                style="margin-left: 6px;font-size:15px;"\n\n              ></ion-icon\n\n              >&nbsp;</span\n\n            >\n\n            <span style="font-size:11px;">{{\n\n              routedata.timestamp | date: "medium"\n\n            }}</span>\n\n          </p>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <p style="font-size: 14px;">\n\n            <span>\n\n              <ion-icon\n\n                ios="ios-pin"\n\n                md="md-pin"\n\n                style="color:#e14444;font-size:17px;margin-left: 8px;\n\n                          "\n\n              ></ion-icon\n\n              >&nbsp;</span\n\n            >\n\n            <span *ngIf="routedata.address"></span>\n\n            <span *ngIf="!routedata.address">&nbsp;&nbsp;N/A</span>\n\n            <span\n\n              style="color:gray;font-size:11px;font-weight: 400;"\n\n              text-nowrap\n\n              >{{ routedata.address }}</span\n\n            >\n\n          </p>\n\n        </ion-row>\n\n      </ion-item>\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/route-voilations/route-voilations.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], RouteVoilationsPage);
    return RouteVoilationsPage;
}());

//# sourceMappingURL=route-voilations.js.map

/***/ })

});
//# sourceMappingURL=33.js.map