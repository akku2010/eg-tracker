webpackJsonp([34],{

/***/ 654:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteMapShowPageModule", function() { return RouteMapShowPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__route_map_show__ = __webpack_require__(766);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RouteMapShowPageModule = /** @class */ (function () {
    function RouteMapShowPageModule() {
    }
    RouteMapShowPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__route_map_show__["a" /* RouteMapShowPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__route_map_show__["a" /* RouteMapShowPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], RouteMapShowPageModule);
    return RouteMapShowPageModule;
}());

//# sourceMappingURL=route-map-show.module.js.map

/***/ }),

/***/ 766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouteMapShowPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { Geolocation } from '@ionic-native/geolocation';
var RouteMapShowPage = /** @class */ (function () {
    function RouteMapShowPage(navCtrl, navParam, apicalligi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.apicalligi = apicalligi;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
        var that = this;
        that.routes = navParam.get("param");
        that.routes = that.routes;
        console.log('Trip Data for polyline', that.routes);
        that.apicalligi.startLoading().present();
        that.apicalligi.route_details(that.routes._id, that.islogin._id)
            .subscribe(function (data) {
            that.routeData = data;
            console.log("RouteData=> " + JSON.stringify(that.routeData));
            that.apicalligi.stopLoading();
            that.mapData = [];
            for (var i = 0; i < that.routeData.routePath.length; i++) {
                that.mapData.push({ "lat": that.routeData.routePath[i].location.coordinates[1], "lng": that.routeData.routePath[i].location.coordinates[0] });
            }
            console.log('Trip Data for polyline', that.mapData);
            var bounds = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            // var center = bounds.getCenter();
            // console.log("bounds=> ", bounds.getCenter())
            // let mapOptions: GoogleMapOptions =
            // {
            //   camera: {
            //     target: center,
            //     zoom: 8,
            //     tilt: 30
            //   }
            // };
            // let mapOptions: GoogleMapOptions = { gestures: { rotate: false, tilt: false, scroll: false } };
            that.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create('showRoute');
            that.map.moveCamera({
                target: bounds
            });
            that.map.addMarker({
                title: 'S',
                position: that.mapData[0],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'red'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
                that.map.addMarker({
                    title: 'D',
                    position: that.mapData[that.mapData.length - 1],
                    icon: 'red',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                }).then(function (marker) {
                    marker.showInfoWindow();
                    // animateMarker(marker, dataArrayCoords, speed, trackerType)
                });
            });
            console.log("latlang.............", that.mapData);
            that.map.addPolyline({
                points: that.mapData,
                // color: '#fa6d29',
                color: 'blue',
                width: 4,
                geodesic: true
            });
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    }
    RouteMapShowPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter RouteMapShowPage');
    };
    RouteMapShowPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-route-map-show',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/route-map-show/route-map-show.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{\'View Route\' | translate}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div id="showRoute"></div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/route-map-show/route-map-show.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], RouteMapShowPage);
    return RouteMapShowPage;
}());

//# sourceMappingURL=route-map-show.js.map

/***/ })

});
//# sourceMappingURL=34.js.map