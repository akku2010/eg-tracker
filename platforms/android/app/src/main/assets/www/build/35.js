webpackJsonp([35],{

/***/ 653:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsCollectionPageModule", function() { return ReportsCollectionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reports_collection__ = __webpack_require__(765);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReportsCollectionPageModule = /** @class */ (function () {
    function ReportsCollectionPageModule() {
    }
    ReportsCollectionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__reports_collection__["a" /* ReportsCollectionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__reports_collection__["a" /* ReportsCollectionPage */]),
            ],
        })
    ], ReportsCollectionPageModule);
    return ReportsCollectionPageModule;
}());

//# sourceMappingURL=reports-collection.module.js.map

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsCollectionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ReportsCollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReportsCollectionPage = /** @class */ (function () {
    function ReportsCollectionPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReportsCollectionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportsCollectionPage');
    };
    ReportsCollectionPage.prototype.acReport = function () {
        this.navCtrl.push("AcReportPage");
    };
    ReportsCollectionPage.prototype.dailyReport = function () {
        this.navCtrl.push("DailyReportPage");
    };
    ReportsCollectionPage.prototype.disance = function () {
        this.navCtrl.push("DistanceReportPage");
    };
    ReportsCollectionPage.prototype.geo = function () {
        this.navCtrl.push("GeofenceReportPage");
    };
    ReportsCollectionPage.prototype.iginition = function () {
        this.navCtrl.push("IgnitionReportPage");
    };
    ReportsCollectionPage.prototype.stoppage = function () {
        this.navCtrl.push("StoppagesRepoPage");
    };
    ReportsCollectionPage.prototype.trip = function () {
        this.navCtrl.push("TripReportPage");
    };
    ReportsCollectionPage.prototype.working = function () {
        this.navCtrl.push("WorkingHoursReportPage");
    };
    ReportsCollectionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-reports-collection',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/reports-collection/reports-collection.html"*/'<!--\n  Generated template for the ReportsCollectionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reports</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)= "acReport()">\n          <div class="style">\n            <img src="assets/imgs/ac.png">\n            <p style="margin: auto;padding: 3px;"> AC Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="dailyReport()">\n          <div class="style">\n            <img src="assets/imgs/24-hour-daily-service.png">\n            <p style="margin: auto;padding: 3px;"> Daily Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="disance()">\n          <div class="style" >\n            <img src="assets/imgs/distance.png">\n            <p style="margin: auto;padding: 3px;"> Distance Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="geo()">\n          <div class="style">\n            <!-- <img src="assets/imgs/ac.png"> -->\n            <ion-icon  name="disc"></ion-icon>\n            <p style="margin: auto;padding: 3px;"> Geofence Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="iginition()">\n          <div class="style">\n            <img src="assets/imgs/power-button.png">\n            <p style="margin: auto;padding: 3px;"> Ignition Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="stoppage()">\n          <div class="style" >\n            <img src="assets/imgs/stoppage.png">\n            <p style="margin: auto;padding: 3px;"> Stoppage Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="trip()">\n          <div class="style">\n            <img src="assets/imgs/destination.png">\n            <p style="margin: auto;padding: 3px;"> Trip Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="working()">\n          <div class="style">\n            <img src="assets/imgs/working-time.png">\n            <p style="margin: auto;padding: 3px;"> Working Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/reports-collection/reports-collection.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ReportsCollectionPage);
    return ReportsCollectionPage;
}());

//# sourceMappingURL=reports-collection.js.map

/***/ })

});
//# sourceMappingURL=35.js.map