webpackJsonp([37],{

/***/ 651:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifSettingPageModule", function() { return NotifSettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notif_setting__ = __webpack_require__(763);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { MatDatepickerModule } from '@angular/material/datepicker';
var NotifSettingPageModule = /** @class */ (function () {
    function NotifSettingPageModule() {
    }
    NotifSettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notif_setting__["a" /* NotifSettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__notif_setting__["a" /* NotifSettingPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
            ],
        })
    ], NotifSettingPageModule);
    return NotifSettingPageModule;
}());

//# sourceMappingURL=notif-setting.module.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotifSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notif_modal__ = __webpack_require__(450);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotifSettingPage = /** @class */ (function () {
    function NotifSettingPage(navCtrl, navParams, apiCall, toastCtrl, modalCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.fData = {};
        this.isAddEmail = false;
        this.isAddPhone = false;
        this.shownotifdiv = true;
        this.newArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.getCustDetails();
    }
    NotifSettingPage.prototype.getCustDetails = function () {
        var _this = this;
        var _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            _this.lt = respData.cust.pass;
            localStorage.setItem('pwd2', _this.lt);
            if (respData.length === 0) {
                return;
            }
            else {
                if (respData.cust.alert) {
                    var result = Object.keys(respData.cust.alert).map(function (key) {
                        return [key, respData.cust.alert[key]];
                    });
                    _this.newArray = result.map(function (r) {
                        return {
                            key: r[0],
                            imgUrl: 'assets/notificationIcon/' + r[0] + '.png',
                            keys: r[1]
                        };
                    });
                    console.log("someArr: " + _this.newArray);
                }
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    NotifSettingPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter NotifSettingPage');
    };
    NotifSettingPage.prototype.onChange = function (ev) {
        console.log("event: ", ev);
    };
    NotifSettingPage.prototype.tab = function (key1, key2) {
        var _this = this;
        if (key1 && key2) {
            if (key1.keys[key2] === false) {
                key1.keys[key2] = false;
            }
            else {
                key1.keys[key2] = true;
            }
        }
        for (var t = 0; t < this.newArray.length; t++) {
            if (this.newArray[t].key === key1.key) {
                this.newArray[t].keys = key1.keys;
            }
        }
        var temp = [];
        for (var e = 0; e < this.newArray.length; e++) {
            temp.push((_a = {},
                _a[this.newArray[e].key] = this.newArray[e].keys,
                _a.key = this.newArray[e].key,
                _a));
        }
        /* covert an array into object */
        var res = {};
        temp.forEach(function (a) { res[a.key] = a[a.key]; });
        this.fData.contactid = this.islogin._id;
        this.fData.alert = res;
        var url = this.apiCall.mainUrl + 'users/editUserDetails';
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, this.fData)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("check stat: ", respData);
            if (respData) {
                _this.toastCtrl.create({
                    message: 'Setting Updated',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
        var _a;
    };
    NotifSettingPage.prototype.addEmail = function (noti) {
        // use AudioProvider to control selected track
        this.notifType = noti.key;
        this.isAddEmail = true;
        var data = {
            "buttonClick": 'email',
            "notifType": this.notifType,
            "compData": (_a = {},
                _a[this.notifType] = noti.keys,
                _a),
            "newArray": this.newArray
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notif_modal__["a" /* NotifModalPage */], {
            "notifData": data
        });
        modal.present();
        var _a;
    };
    NotifSettingPage.prototype.addPhone = function (noti) {
        this.notifType = noti.key;
        this.isAddPhone = true;
        var data = {
            "buttonClick": 'phone',
            "notifType": this.notifType,
            "compData": (_a = {},
                _a[this.notifType] = noti.keys,
                _a),
            "newArray": this.newArray
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notif_modal__["a" /* NotifModalPage */], {
            "notifData": data
        });
        modal.present();
        var _a;
    };
    NotifSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notif-setting',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/profile/settings/notif-setting/notif-setting.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ "Notification Settings"  | translate}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-card *ngFor="let item of newArray">\n\n    <ion-card-content>\n\n      <ion-row>\n\n        <ion-col col-1 style="margin: auto;">\n\n          <img [src]="item.imgUrl" style="width: 20px; height: 20px;" />\n\n        </ion-col>\n\n        <ion-col col-3 style="margin: auto;">\n\n          <p style="padding: 6px 0px 0px 0px; color: #000; font-size: 12px;\n\n          font-weight: 500;">{{item.key | titlecase}}</p>\n\n        </ion-col>\n\n        <ion-col col-6 no-padding>\n\n          <ion-row style="margin-top: -10px;">\n\n            <ion-col col-4 no-padding>\n\n              <ion-item no-padding>\n\n                <ion-label style="font-size: 12px;">{{ \'Email\' | translate }}</ion-label>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 no-padding>\n\n              <ion-item no-padding>\n\n                <ion-label style="font-size: 12px;">{{ \'SMS\' | translate }}</ion-label>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 no-padding>\n\n              <ion-item no-padding>\n\n                <ion-label style="font-size: 12px;">{{ \'Notification\' | translate }}</ion-label>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row style="margin-top: -21px;margin-bottom: -21px;">\n\n            <ion-col col-4 style="padding-left: 0px;">\n\n              <ion-item no-padding>\n\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.email_status"\n\n                  *ngIf="islogin.isSuperAdmin || islogin.isDealer" (ionChange)="tab(item,\'email_status\')"></ion-toggle>\n\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.email_status"\n\n                  *ngIf="!islogin.isSuperAdmin && !islogin.isDealer" (ionChange)="tab(item,\'email_status\')"\n\n                  disabled="true"></ion-toggle>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 style="padding-left: 0px;">\n\n              <ion-item no-padding>\n\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.sms_status"\n\n                  *ngIf="islogin.isSuperAdmin || islogin.isDealer" (ionChange)="tab(item,\'sms_status\')"></ion-toggle>\n\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.sms_status"\n\n                  *ngIf="!islogin.isSuperAdmin && !islogin.isDealer" (ionChange)="tab(item,\'sms_status\')"\n\n                  disabled="true"></ion-toggle>\n\n              </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 style="padding-left: 0px;">\n\n              <ion-item no-padding>\n\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.notif_status"\n\n                  (ionChange)="tab(item,\'notif_status\')"></ion-toggle>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-1 (click)="addEmail(item)">\n\n          <ion-icon name="mail" color="primary"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-1 (click)="addPhone(item)">\n\n          <ion-icon name="call" color="secondary"></ion-icon>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/profile/settings/notif-setting/notif-setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], NotifSettingPage);
    return NotifSettingPage;
}());

//# sourceMappingURL=notif-setting.js.map

/***/ })

});
//# sourceMappingURL=37.js.map