webpackJsonp([43],{

/***/ 639:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpiredPageModule", function() { return ExpiredPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expired__ = __webpack_require__(751);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ExpiredPageModule = /** @class */ (function () {
    function ExpiredPageModule() {
    }
    ExpiredPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            providers: [
            //  {provide: String, useValue: "SoapService"}
            // SoapService
            ],
        })
    ], ExpiredPageModule);
    return ExpiredPageModule;
}());

//# sourceMappingURL=expired.module.js.map

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpiredPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExpiredPage = /** @class */ (function () {
    function ExpiredPage(params, viewCtrl, toastCtrl, navCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.isPayNow = false;
        if (params.get("param")) {
            this.data = params.get("param");
        }
    }
    ExpiredPage.prototype.activate = function () {
        // this.navCtrl.push("AddDevicesPage");
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": this.data
        });
        console.log("Redirect to paytm page");
    };
    ExpiredPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ExpiredPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expired',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/live/expired/expired.html"*/'<ion-content padding class="guide-modal">\n\n\n\n    <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n    <!-- <div *ngIf="isPayNow " style="display: flex;justify-content: space-around;">\n\n        <button ion-button  (click)="activateVehicles()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n\n        <span>Pay Now</span>\n\n        </button>\n\n        <button ion-button (click)="supportCall()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n\n         <ion-icon ios="ios-call" md="md-call" style="color: white;"></ion-icon>Support\n\n       </button>\n\n      </div>\n\n      <div *ngIf="d.expiration_date < now" style="position: relative">\n\n        <div (click)="activateVehicle(d)">\n\n          <ion-item style="background-color: black">\n\n\n\n            <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)" (click)="selected($event,d)">\n\n              <img *ngIf="d.iconType === \'jjcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}}>\n\n              <img *ngIf="!isSelected(d)" width=\'20px\' style="margin-left: 10px;" src={{devIcon}}>\n\n              <img *ngIf="isSelected(d)" width=\'20px\' style="margin-left: 10px;" src="assets/imgs/cheackright.png">\n\n                <ion-icon ios="ios-add-circle" md="md-add-circle" *ngIf="selectedArray.length > 0" style="color: white; font-size: 20px;"></ion-icon> -->\n\n           <!-- </ion-avatar>\n\n            <div>\n\n              <h2 style="color: gray">{{ d.Device_Name }}</h2>\n\n              <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n\n                <span style="text-transform: uppercase; color: red;">{{ d.status }}\n\n                </span>\n\n                <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n\n                  d.status_updated_at | date: "medium"\n\n                }}\n\n                </span>\n\n              </p>\n\n            </div>\n\n          </ion-item>\n\n        </div>\n\n      </div> -->\n\n\n\n    <div>\n\n        <p style="text-align: center; font-size: 2rem">\n\n            <b>{{\'Device Expired!\' | translate}}</b>\n\n        </p>\n\n        <p>\n\n            <button ion-button block outline color="secondary" (tap)="activate()">{{\'Activate\' | translate}}</button>\n\n        </p>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/live/expired/expired.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], ExpiredPage);
    return ExpiredPage;
}());

//# sourceMappingURL=expired.js.map

/***/ })

});
//# sourceMappingURL=43.js.map