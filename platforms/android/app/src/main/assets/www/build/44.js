webpackJsonp([44],{

/***/ 637:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSettingsPageModule", function() { return DeviceSettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_settings__ = __webpack_require__(750);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DeviceSettingsPageModule = /** @class */ (function () {
    function DeviceSettingsPageModule() {
    }
    DeviceSettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_settings__["a" /* DeviceSettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_settings__["a" /* DeviceSettingsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], DeviceSettingsPageModule);
    return DeviceSettingsPageModule;
}());

//# sourceMappingURL=device-settings.module.js.map

/***/ }),

/***/ 750:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DeviceSettingsPage = /** @class */ (function () {
    function DeviceSettingsPage(navCtrl, navParams, viewCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.dData = {};
        this.fmileage = 0;
        this.showContainer = false;
        this.dData = navParams.get("param");
        console.log("param data: ", this.dData);
        this.vname = this.dData.Device_Name;
        this.tot_odo = this.fixDecimals(this.dData.total_odo); // for two decimals
        this.speedlimit = this.dData.overStoppedLimit;
        this.ingnitionStat = this.dData.ignitionSource;
        this.fmileage = this.dData.Mileage;
        if (this.fmileage === undefined) {
            this.fmileage = 0;
        }
        ;
        debugger;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        if (localStorage.getItem("custumer_status") == 'OFF' || localStorage.getItem("custumer_status") == null) {
            if (this.islogin.isSuperAdmin == true || this.islogin.isDealer == true) {
                this.showContainer = true;
            }
        }
    }
    DeviceSettingsPage.prototype.fixDecimals = function (value) {
        value = "" + value;
        value = value.trim();
        value = parseFloat(value).toFixed(2);
        return value;
    };
    DeviceSettingsPage.prototype.radioChecked = function (key) {
        console.log("ignition key=> ", key);
    };
    DeviceSettingsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DeviceSettingsPage.prototype.submitSettings = function () {
        var _this = this;
        if (this.speedlimit === undefined || this.ingnitionStat === undefined || this.vname === undefined || this.fmileage === undefined) {
            this.viewCtrl.dismiss();
        }
        else {
            var editData = {
                _id: this.dData._id,
                deviceid: this.dData.Device_ID,
                devicename: this.vname,
                speed: this.speedlimit,
                ignitionSource: this.ingnitionStat,
                total_odo: this.tot_odo,
                Mileage: this.fmileage
            };
            this.apiCall.startLoading().present();
            this.apiCall.deviceupdateCall(editData)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                // console.log("resp data=> " + data.message)
                var toast = _this.toastCtrl.create({
                    message: data.message + " successfully!",
                    duration: 1500,
                    position: "bottom"
                });
                toast.onDidDismiss(function () {
                    _this.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                console.log(err);
                _this.apiCall.stopLoading();
            });
        }
    };
    DeviceSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-settings',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/live-single-device/device-settings/device-settings.html"*/'<!-- <ion-content padding> -->\n\n  <div padding>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <p style="text-align: center; font-size: 2rem;">\n\n          <b>{{\'Device Settings\' | translate}}</b>\n\n         &nbsp;&nbsp;&nbsp; <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </p>\n\n      </ion-col>\n\n    </ion-row>\n\n    <br />\n\n    <ion-row>\n\n      <ion-col col-12><b>{{\'Vehicle Name:\' | translate}}</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="text" [(ngModel)]="vname"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12><b>{{\'Total ODO:\' | translate}}</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="text" [(ngModel)]="tot_odo"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12><b>{{\'Fuel Mileage:\' | translate}}</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="number" [(ngModel)]="fmileage"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-6><b>{{\'Speed Limit\' | translate}}:</b></ion-col>\n\n      <ion-col col-6 style="text-align: right">\n\n        <ion-badge color="gpsc" item-end>{{speedlimit}}</ion-badge>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-range min="10" max="500" step="2" color="gpsc" [(ngModel)]="speedlimit">\n\n          <ion-icon small range-left name="speedometer"></ion-icon>\n\n          <ion-icon range-right name="speedometer"></ion-icon>\n\n        </ion-range>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ng-container *ngIf="showContainer == true">\n\n    <ion-row padding-bottom>\n\n      <ion-col col-12><b>{{\'Ignition Detection:\' | translate}}:</b></ion-col>\n\n    </ion-row>\n\n    <ion-row radio-group [(ngModel)]="ingnitionStat">\n\n      <ion-col col-6>\n\n        <ion-label>{{\'Movement\' | translate}}</ion-label>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: right;">\n\n        <ion-radio color="gpsc" value="MOVEMENT" (ionSelect)="radioChecked(ingnitionStat)"></ion-radio>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row radio-group [(ngModel)]="ingnitionStat">\n\n      <ion-col col-6>\n\n        <ion-label>{{\'ACC\' | translate}}</ion-label>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: right;">\n\n        <ion-radio color="gpsc" value="ACC" (ionSelect)="radioChecked(ingnitionStat)"></ion-radio>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ng-container>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <button ion-button block (click)="submitSettings()" color="gpsc">{{\'SUBMIT\' | translate}}</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n<!-- </ion-content> -->\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/live-single-device/device-settings/device-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DeviceSettingsPage);
    return DeviceSettingsPage;
}());

//# sourceMappingURL=device-settings.js.map

/***/ })

});
//# sourceMappingURL=44.js.map