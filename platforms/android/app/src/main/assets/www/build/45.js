webpackJsonp([45],{

/***/ 635:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdleReportPageModule", function() { return IdleReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__idle_report__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var IdleReportPageModule = /** @class */ (function () {
    function IdleReportPageModule() {
    }
    IdleReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__idle_report__["a" /* IdleReportPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__idle_report__["a" /* IdleReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], IdleReportPageModule);
    return IdleReportPageModule;
}());

//# sourceMappingURL=idle-report.module.js.map

/***/ }),

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdleReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IdleReportPage = /** @class */ (function () {
    function IdleReportPage(navCtrl, navParams, apiCall, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.device_id = [];
        this.minTime = 5;
        this.pData = [];
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    IdleReportPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter IdleReportPage');
    };
    IdleReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    IdleReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    IdleReportPage.prototype.getReport = function () {
        var _this = this;
        if (this.device_id.length === 0) {
            this.toastCtrl.create({
                message: 'Please select vehicle and try again..',
                duration: 1500,
                position: 'bottom'
            }).present();
            return;
        }
        var _bUrl = this.apiCall.mainUrl + 'stoppage/idleReportdatatable';
        var payload = {
            "draw": 3,
            "columns": [
                {
                    "data": "_id"
                },
                {
                    "data": "device"
                },
                {
                    "data": "device.Device_Name"
                },
                {
                    "data": "start_time"
                },
                {
                    "data": "end_time"
                },
                {
                    "data": "lat"
                },
                {
                    "data": "long"
                },
                {
                    "data": "ac_status"
                },
                {
                    "data": "idle_time"
                },
                {
                    "data": "address"
                },
                {
                    "data": null,
                    "defaultContent": ""
                }
            ],
            "order": [
                {
                    "column": 0,
                    "dir": "asc"
                }
            ],
            "start": 0,
            "length": 10,
            "search": {
                "value": "",
                "regex": false
            },
            "op": {},
            "select": [],
            "find": {
                "device": {
                    "$in": this.device_id
                },
                "start_time": {
                    "$gte": new Date(this.datetimeStart).toISOString()
                },
                "end_time": {
                    "$lte": new Date(this.datetimeEnd).toISOString()
                },
                "idle_time": {
                    "$gte": (this.minTime * 60000)
                }
            }
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_bUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log('idle report data: ', data);
            if (data.length > 0) {
                _this.innerFunc(data);
            }
        });
    };
    IdleReportPage.prototype.innerFunc = function (pdata) {
        var outerthis = this;
        var i = 0, howManyTimes = pdata.length;
        function f() {
            // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
            // var hourconversion = 2.7777778 / 10000000;
            outerthis.pData.push({
                'Device_Name': pdata[i].device.Device_Name,
                'start_location': {
                    'lat': pdata[i].lat,
                    'lng': pdata[i].long
                },
                'ac_status': (pdata[i].ac_status ? pdata[i].ac_status : 'NA'),
                'end_time': pdata[i].end_time,
                'duration': outerthis.parseMillisecondsIntoReadableTime(pdata[i].idle_time),
                'start_time': pdata[i].start_time
            });
            outerthis.start_address(outerthis.pData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    IdleReportPage.prototype.start_address = function (item, index) {
        var _this = this;
        var that = this;
        //that.pData[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.pData[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            // debugger
            var payload = {
                "lat": item.start_location.lat,
                "long": item.start_location.lng,
            };
            this.apiCall.getAddress(payload)
                .subscribe(function (res) {
                if (res.message == "Address not found in databse") {
                    _this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.lng)
                        .then(function (res) {
                        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                        that.saveAddressToServer(str, item.start_location.lat, item.start_location.lng);
                        that.pData[index].StartLocation = str;
                        console.log("inside", that.pData[index].StartLocation);
                    });
                }
                else {
                    that.pData[index].StartLocation = res.address;
                }
            });
        }
    };
    IdleReportPage.prototype.getDeviceDetail = function (selectedVehicle) {
        // console.log(devData);
        // this.device_id = devData;
        this.device_id = [];
        if (selectedVehicle.length > 0) {
            if (selectedVehicle.length > 1) {
                for (var t = 0; t < selectedVehicle.length; t++) {
                    this.device_id.push(selectedVehicle[t]._id);
                }
            }
            else {
                this.device_id.push(selectedVehicle[0]._id);
            }
        }
        else
            return;
    };
    IdleReportPage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    IdleReportPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    IdleReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-idle-report',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/idle-report/idle-report.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Idle Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="height: 45px; background-color: #fafafa;">\n\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getDeviceDetail(selectedVehicle)" [isMultiple]="true">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Min Idle Time(Min)</ion-label>\n\n    <ion-input type="number" [(ngModel)]="minTime"></ion-input >\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater" [max]="today" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getReport()">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let item of pData; let i = index">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png" />\n\n      </ion-avatar>\n\n      <ion-label>{{ item.Device_Name }}</ion-label>\n\n      <ion-badge item-end color="gpsc">AC Status - {{ item.ac_status}}</ion-badge>\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row style="padding-top: 12px;">\n\n        <ion-col col-4>\n\n          <p style="color:#53ab53;font-size:11px;font-weight: bold;">\n\n            Start Time\n\n          </p>\n\n\n\n        </ion-col>\n\n        <ion-col col-8>\n\n          <p class="para">\n\n            {{item.start_time | date: \'mediumDate\'}}, {{item.start_time | date:\'shortTime\'}}\n\n          </p>\n\n\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-4>\n\n          <p style="text-align:left;font-size: 11px;color:red;font-weight:bold;">\n\n            End Time\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-8>\n\n          <p class="para">\n\n            {{item.end_time | date:\'mediumDate\'}}, {{item.end_time | date:\'shortTime\'}}\n\n          </p>\n\n        </ion-col>\n\n\n\n      </ion-row>\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-4>\n\n          <p style="text-align:left;font-size: 11px;color:#009688;font-weight:bold;">\n\n            Duration\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-8>\n\n          <p class="para">\n\n            {{item.duration}}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-1>\n\n          <ion-icon name="pin" style="color:#33c45c; font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 (onCreate)="start_address(item, i)" style="color:gray;font-size:11px;font-weight: 400;">\n\n          {{ item.StartLocation }}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/idle-report/idle-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], IdleReportPage);
    return IdleReportPage;
}());

//# sourceMappingURL=idle-report.js.map

/***/ })

});
//# sourceMappingURL=45.js.map