webpackJsonp([60],{

/***/ 610:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCustModalPageModule", function() { return UpdateCustModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_cust__ = __webpack_require__(712);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var UpdateCustModalPageModule = /** @class */ (function () {
    function UpdateCustModalPageModule() {
    }
    UpdateCustModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__update_cust__["a" /* UpdateCustModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__update_cust__["a" /* UpdateCustModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"]
            ]
        })
    ], UpdateCustModalPageModule);
    return UpdateCustModalPageModule;
}());

//# sourceMappingURL=update-cust.module.js.map

/***/ }),

/***/ 712:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateCustModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UpdateCustModalPage = /** @class */ (function () {
    function UpdateCustModalPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl, translate) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.translate = translate;
        this.getAllDealersData = [];
        this.devicedetail = {};
        this.customerdata = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.customer = navPar.get("param");
        if (this.customer.expiration_date == null) {
            var tru = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            tempdate.setDate(tempdate.getDate() + 365);
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        }
        else {
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.expiration_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }
        // ============== one month later date from current date ================
        debugger;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;
        this.minDate = today;
        console.log("minimum date: ", today);
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this.customer.first_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this.customer.last_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [this.customer.email],
            phone: [this.customer.phone],
            address: [this.customer.address],
            creationdate: [__WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD')],
            expirationdate: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        });
        // this.updatecustForm.patchValue()
    }
    UpdateCustModalPage.prototype.ngOnInit = function () {
        this.getAllDealers();
    };
    UpdateCustModalPage.prototype.getAllDealers = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        var toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apiCall.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.getAllDealersData = data;
        }, function (error) {
            console.log(error);
        });
    };
    UpdateCustModalPage.prototype.DealerselectData = function (dealerselect) {
        //this.dealerdata = dealerselect;
        this.dealerdata = this.updatecustForm.value.dealer_firstname;
    };
    UpdateCustModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateCustModalPage.prototype.updateCustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                "expire_date": new Date(this.updatecustForm.value.expirationdate).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid,
                "email": this.updatecustForm.value.email,
                "phone": this.updatecustForm.value.phone
            };
            if (this.dealerdata != undefined) {
                this.devicedetail.Dealer = this.dealerdata.dealer_id;
            }
            // else {
            //     this.devicedetail.Dealer = this.islogin._id;
            // }
            if (this.vehType == undefined) {
                this.devicedetail;
            }
            else {
                this.devicedetail.vehicleType = this.vehType._id;
            }
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.editdata = data;
                var toast = _this.toastCtrl.create({
                    message: _this.translate.instant('dealerUpdated', { value: _this.translate.instant("Customers") }),
                    position: 'bottom',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    _this.viewCtrl.dismiss(_this.editdata);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = _this.alerCtrl.create({
                    message: msg.message,
                    buttons: [_this.translate.instant('Okay')]
                });
                alert.present();
            });
        }
    };
    UpdateCustModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/customers/modals/update-cust/update-cust.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Edit Customer Details\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button ion-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="updatecustForm">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'User ID*\' | translate}}</ion-label>\n\n            <ion-input formControlName="userid" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.userid.valid && (updatecustForm.controls.userid.dirty || submitAttempt)">\n\n            <p>{{\'user id required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'First Name*\' | translate}}</ion-label>\n\n            <ion-input formControlName="first_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.first_name.valid && (updatecustForm.controls.first_name.dirty || submitAttempt)">\n\n            <p>{{\'first name required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Last Name*\' | translate}}</ion-label>\n\n            <ion-input formControlName="last_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.last_name.valid && (updatecustForm.controls.last_name.dirty || submitAttempt)">\n\n            <p>{{\'last name required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Email ID\' | translate}}</ion-label>\n\n            <ion-input formControlName="email" type="email"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.email.valid && (updatecustForm.controls.email.dirty || submitAttempt)">\n\n            <p>{{\'email id required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Mobile Number\' | translate}}</ion-label>\n\n            <ion-input formControlName="phone" type="number"></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!updatecustForm.controls.phone.valid && (updatecustForm.controls.phone.dirty || submitAttempt)">\n\n            <p>{{\'mobile number required and should be 10 digits!\' | translate}}</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Address\' | translate}}</ion-label>\n\n            <ion-input formControlName="address" type="text"></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!updatecustForm.controls.address.valid && (updatecustForm.controls.address.dirty || submitAttempt)">\n\n            <p>{{\'Address required!\' | translate}}</p>\n\n        </ion-item> -->\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Created On:\' | translate}}</ion-label>\n\n            <ion-input type="date" formControlName="creationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Expire On*\' | translate}}</ion-label>\n\n            <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>{{\'Dealers\' | translate}}</ion-label>\n\n            <!-- <ion-select formControlName="dealer_firstname" style="min-width:49%;">\n\n                <ion-option *ngFor="let dealer of getAllDealersData" value="dealer.dealer_firstname" (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname | titlecase}} {{dealer.dealer_lastname | titlecase}}</ion-option>\n\n            </ion-select> -->\n\n            <select-searchable item-content formControlName="dealer_firstname" [items]="getAllDealersData"\n\n            itemValueField="dealer_firstname" itemTextField="dealer_firstname" [canSearch]="true"\n\n            (onChange)="DealerselectData(dealer_firstname)" style="padding: 0px;margin-top: -1%;">\n\n        </select-searchable>\n\n        </ion-item>\n\n        <!-- <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>{{\'Dealers\' | translate}}</ion-label>\n\n            <ion-select formControlName="dealer_firstname">\n\n                <ion-option *ngFor="let dealer of getAllDealersData" [value]="dealer.dealer_firstname" (ionSelect)="DealerselectData(dealer)">{{dealer.dealer_firstname}}</ion-option>\n\n            </ion-select>\n\n        </ion-item> -->\n\n    </form>\n\n    <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="updateCustomer()">{{\'UPDATE CUSTOMER DETAILS\' | translate}}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/eg-tracker/src/pages/customers/modals/update-cust/update-cust.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], UpdateCustModalPage);
    return UpdateCustModalPage;
}());

//# sourceMappingURL=update-cust.js.map

/***/ })

});
//# sourceMappingURL=60.js.map