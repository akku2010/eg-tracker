import { Component, Input } from '@angular/core';

@Component({
  selector: 'reports-header',
  templateUrl: 'reports-header.html'
})
export class ReportsHeaderComponent {
  @Input() reportName :any;
  text: string;

  constructor() {
    console.log('Hello ReportsHeaderComponent Component');
    this.text = 'Hello World';
    console.log("check parent data: ", this.reportName);
  }

}
