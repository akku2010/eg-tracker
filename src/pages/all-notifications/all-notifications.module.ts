import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllNotificationsPage } from './all-notifications';
import { SelectSearchableModule } from '../../../node_modules/ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AllNotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllNotificationsPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class AllNotificationsPageModule {}
