import { Component } from "@angular/core";
import { ViewController, NavParams } from "ionic-angular";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import * as moment from 'moment';


@Component({
  template: `

<ion-list>
  <div class = "mainDiv">
          <ion-buttons end>
            <button ion-button icon-only (click)="dismiss()">
                <ion-icon name="close-circle"></ion-icon>
            </button>
          </ion-buttons>
      <div class="secondDiv">
      <!-- <img ng-src="../assets/imgs/tenor.gif" alt="Description" /> -->
      <!-- <span style="text-align: center">Upcoming Expired Devices</span> -->
      <p style="text-align: center;
    font-family: initial;
    font-size: 0.85 em; color: black">Reminders</p>
      <ion-row *ngFor="let d of reminders">
        <ion-col style="font-size: 0.85em;color: black">
            {{d}}
        </ion-col>
        <!-- <ion-col style="font-size: 0.85em;" >
          {{d.device_name}}
        </ion-col>
        <ion-col style="font-size: 0.85em">
        {{d.expDate}}
        </ion-col> -->
       </ion-row>


      </div>
  </div>
</ion-list>
  `,

  styles: [`

.mainDiv {
    padding: 50px;
    height: 100%;
    margin-top: 17px;
    /* background-color: rgba(0, 0, 0, 0.7); */

    /* //background: url("../assets/imgs/don-t-forget.jpg")
      no-repeat center center;
    background-repeat: no-repeat;
    background-size: cover;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    text-align: center; */
}
 .secondDiv {
     padding-top: 20px;
      border-radius: 18px;
     /* border: 2px solid black; */
     background: white;
     height: 500px !important;
     overflow: scroll;
     /* width: max-content; */
 }


     /* .scroll-content {
       left: 0;
     right: 0;
     top: 0;
      bottom: 0;
      position: absolute;
      z-index: 1;
      display: block;
      overflow-x: hidden;
       overflow-y: hidden;
      -webkit-overflow-scrolling: touch;
      will-change: scroll-position;
       contain: size style layout;
   } */

`]
})

export class AlertModelPage {
  min_time: any = "10";
  page: number = 0;
  limit: number = 8;
  stausdevice: string;
  datetimeStart: string;
  datetime: string;
  allDevices: any = []
  exp_devices: any = [];
  final: any = [];
  start: any;
  end: any;
  islogin: any;
  checkIfStat: string = "ALL";
  obj: any = {};
  finalData = [];
  reminders: any;

  constructor(
    navParams: NavParams,
    private viewCtrl: ViewController,
    public apiCall: ApiServiceProvider
  ) {
    console.log("final sub", this.finalData)
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
      this.checkIfStat = this.stausdevice;
    } else {
      this.stausdevice = undefined;
    }
    console.log("navparam test: " + navParams.get("params"));
    this.obj = navParams.get("params");
    this.datetime = moment({ hours: 0 }).format('LLLL');
    console.log("current date =>", this.datetime);
    this.start = this.datetime;
    this.datetimeStart = moment({ hours: 0 }).add(6, 'days').format('LLLL');
    console.log("upcoming week date for expired device =>", this.datetimeStart);
    this.end = this.datetimeStart
    console.log("start date", this.start);

    console.log("end date", this.end);
    this.popUpForExpiredDevices();
    this.setCookie('displayedPopupNewsletter', 'yes', 1);


  }

  setCookie(c_name, value, exdays) {
    var c_value = escape(value);
    if (exdays) {
      var exdate = new Date();
      exdate.setDate(exdate.getDate() + exdays);
      c_value += '; expires=' + exdate.toUTCString();
    }
    document.cookie = c_name + '=' + c_value;
  }

  getCookie(c_name) {
    var i, x, y, cookies = document.cookie.split(';');

    for (i = 0; i < cookies.length; i++) {
      x = cookies[i].substr(0, cookies[i].indexOf('='));
      y = cookies[i].substr(cookies[i].indexOf('=') + 1);
      x = x.replace(/^\s+|\s+$/g, '');

      if (x === c_name) {
        return unescape(y);
      }
    }
  }



  popUpForExpiredDevices() {
    var baseUrl = "https://www.oneqlik.in/reminder/getReminder?id=" + this.islogin._id;
    this.apiCall.getdevicesForAllVehiclesApi(baseUrl)
      .subscribe(resp => {
        console.log("chk reminders", resp);
        this.reminders = resp
      })
  }
  dismiss() {
    console.log("Inside the function");
    this.viewCtrl.dismiss();
  }
}
