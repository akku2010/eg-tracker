import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeviceSummaryRepoPage } from './device-summary-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
@NgModule({
  declarations: [
    DeviceSummaryRepoPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(DeviceSummaryRepoPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],

exports: [
  OnCreate
],
})
export class DeviceSummaryRepoPageModule {}
