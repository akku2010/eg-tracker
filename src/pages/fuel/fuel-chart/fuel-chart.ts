import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
// import { ScreenOrientation } from '@ionic-native/screen-orientation';

@IonicPage()
@Component({
  selector: 'page-fuel-chart',
  templateUrl: 'fuel-chart.html',
})
export class FuelChartPage implements OnInit, OnDestroy {
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  portstemp: any[] = [];
  _vehId: any;
  selectedVehicle: any;
  @ViewChild("lineCanvas") lineCanvas: ElementRef;
  // private lineChart: Chart;

  constructor(
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController, public navParams: NavParams,
    private screenOrientation: ScreenOrientation
  ) {
    // get current
    console.log(this.screenOrientation.type); // logs the current orientation, example: 'landscape'

    // set to landscape
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();
    // get current
    // console.log(this.screenOrientation.type); // logs the current orientation, example: 'landscape'
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter FuelChartPage');
  }

  ngOnInit() {
    this.getdevices();
  }

  ngOnDestroy() {
    // set to landscape
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getid(veh) {
    this._vehId = veh.Device_ID;
  }

  InsertionTime = [];
  fuelGraphData = [];
  fuelVoltage = [];
  getFueldata() {
    this.InsertionTime = [];
    this.fuelGraphData = [];
    this.fuelVoltage = [];

    if (this._vehId == undefined) {
      let toast = this.toastCtrl.create({
        message: 'Please select the vehicle first.',
        duration: 1500,
        position: 'bottom'
      })
      toast.present();
      return
    }
    var url = this.apiCall.mainUrl + "summary/fuel?i=" + this._vehId + "&f=" + new Date(this.datetimeStart).toISOString() + "&t=" + new Date(this.datetimeEnd).toISOString();
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(url)
      .subscribe(res => {
        this.apiCall.stopLoading();
        // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
        let graphData = res;
        for (var i = graphData.length - 1; i >= 0; i--) {
          var fuelData = graphData[i].currentFuel ? graphData[i].currentFuel : 0;
          var voltageData = graphData[i].fuelVoltage ? parseFloat(graphData[i].fuelVoltage) / 1000 : 0;
          this.fuelVoltage.push(voltageData);
          this.fuelGraphData.push(fuelData);
          var iTime = moment(new Date(graphData[i].insertionTime), "DD/MM/YYYY").format("DD MMM YY");
          this.InsertionTime.push(iTime)
        }
        this.dataToPlot();
      },
        err => {
          this.apiCall.stopLoading();
          console.log("getting error: ", err)
        })
  }

  dataToPlot() {
    this.fuelVoltage
    this.fuelGraphData
    this.InsertionTime
    var fuelChart;
    // var that = this;
    var canvas = <HTMLCanvasElement>document.getElementById("fuelChart");
    var ctx = canvas.getContext("2d");
    fuelChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.InsertionTime,
        datasets: [{
          label: 'Fuel',
          data: this.fuelGraphData,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1,
          spanGaps: true
        },
        {
          label: 'Voltage',
          data: this.fuelVoltage,
          backgroundColor: [
            'rgba(73,188,170,0.4)'
          ],
          borderColor: [
            'rgba(73,188,170,1)',

          ],
          borderWidth: 1,
          spanGaps: true
        }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        zoom: {
          enabled: true,
          drag: true,
          rotate: true,
          mode: "x",
          speed: 1
          // speed: 0.01,
          // sensitivity: 0.1,
          // limits: {
          //   max: 10,
          //   min: 0.5
          // }
        }
      }
    });
    console.log((fuelChart));

  }
}
