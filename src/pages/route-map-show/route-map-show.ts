import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps, Marker, LatLngBounds } from '@ionic-native/google-maps';
// import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-route-map-show',
  templateUrl: 'route-map-show.html',
})
export class RouteMapShowPage {
  routes: any;
  routeData: any;
  islogin: any;
  mapData: any[];

  latLng: any;
  mapOptions: { backgroundColor: string; };
  map: any;

  constructor(public navCtrl: NavController, navParam: NavParams, public apicalligi: ApiServiceProvider, 
    // public geolocation: Geolocation
    ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log(this.islogin._id);

    let that = this;
    that.routes = navParam.get("param");
    that.routes = that.routes;

    console.log('Trip Data for polyline', that.routes);

    that.apicalligi.startLoading().present();
    that.apicalligi.route_details(that.routes._id, that.islogin._id)
      .subscribe(data => {
        that.routeData = data;
        console.log("RouteData=> " + JSON.stringify(that.routeData));
        that.apicalligi.stopLoading();

        that.mapData = [];
        for (var i = 0; i < that.routeData.routePath.length; i++) {
          that.mapData.push({ "lat": that.routeData.routePath[i].location.coordinates[1], "lng": that.routeData.routePath[i].location.coordinates[0] });

        }
        console.log('Trip Data for polyline', that.mapData);

        let bounds = new LatLngBounds(that.mapData);

        // var center = bounds.getCenter();
        // console.log("bounds=> ", bounds.getCenter())

        // let mapOptions: GoogleMapOptions =
        // {
        //   camera: {
        //     target: center,
        //     zoom: 8,
        //     tilt: 30
        //   }
        // };

        // let mapOptions: GoogleMapOptions = { gestures: { rotate: false, tilt: false, scroll: false } };
        that.map = GoogleMaps.create('showRoute');
        that.map.moveCamera({
          target: bounds
        })

        that.map.addMarker({
          title: 'S',
          position: that.mapData[0],
          icon: 'green',
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'red'
          },
          // icon: 'http://www.googlemapsmarkers.com/v1/FF0000/'
        }).then((marker: Marker) => {
          marker.showInfoWindow();

          that.map.addMarker({
            title: 'D',
            position: that.mapData[that.mapData.length - 1],
            icon: 'red',
            styles: {
              'text-align': 'center',
              'font-style': 'italic',
              'font-weight': 'bold',
              'color': 'green'
            },
            // icon: 'http://www.googlemapsmarkers.com/v1/009900/'
          }).then((marker: Marker) => {
            marker.showInfoWindow();
            // animateMarker(marker, dataArrayCoords, speed, trackerType)
          });
        });

        console.log("latlang.............", that.mapData)
        that.map.addPolyline({
          points: that.mapData,
          // color: '#fa6d29',
          color: 'blue',
          width: 4,
          geodesic: true
        })
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter RouteMapShowPage');
  }

}
