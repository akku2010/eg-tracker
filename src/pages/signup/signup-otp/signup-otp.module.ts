import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupOtpPage } from './signup-otp';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SignupOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupOtpPage),
    TranslateModule.forChild()
  ],
})
export class SignupOtpPageModule {}
